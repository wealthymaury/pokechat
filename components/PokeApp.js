import React from 'react';
import PokeTable from './PokeTable';
import PokeChat from './PokeChat';
import uid from 'uid';

export default class PokeApp extends React.Component {
	constructor(props) {
		super(props);
		this.state = { 
			messages: [],
			pokemons: [
				{ number: 1, name: "Bulbasaur" },
				{ number: 2, name: "Ivysaur" },
				{ number: 3, name: "Venusaur" },
			]
		};
		//this.onGrowl = this.onGrowl.bind(this);
	}

	onGrowl(name) {
		let message = `${name}, ${name}!`;
		let messages = this.state.messages;
		let objMessage = { 
			id: uid(), 
			text: message 
		};
		messages.push(objMessage);
		this.setState({ messages: messages });
	}

	render() {
		return <div className="PokeApp">
			<PokeTable pokemons={this.state.pokemons} onGrowl={this.onGrowl.bind(this)} />
			<PokeChat messages={this.state.messages} />
		</div>
	}
}