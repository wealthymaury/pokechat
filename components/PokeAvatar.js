/*
 * Module dependencies
 * asi se importan modulos en ecmascript 6, 
 * con esto ya no necesitarias jalar todos los archivos con etiquetas script
 */

import React from 'react'; // lo importamos desde la carpeta node_modules :D :'D

class PokeAvatar extends React.Component{
	render() {
		var url = `http://veekum.com/dex/media/pokemon/main-sprites/x-y/${this.props.number}.png`;
		return <img src={url} className="avatar" />
	}
}

export default PokeAvatar; // asi cuando importemos este modulo en otro lugar, lo que se va a importar es la clase que acabamos de definir